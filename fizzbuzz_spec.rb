require_relative 'fizzbuzz'

require 'simplecov'
SimpleCov.start


describe FizzBuzz do
  subject { FizzBuzz.new }

  describe '#divisble_by' do
    let(:denominator) { 3 }
    context 'when number is not divisible' do
      let(:numerator) { 5 }
      it 'returns false' do
        expect(subject.divisible_by?(numerator, denominator)).to be_falsy
      end
    end

    context 'when number is divisible' do
      let(:numerator) { 3 }
      it 'returns true' do
        expect(subject.divisible_by?(numerator, denominator)).to be_truthy
      end
    end
  end

  describe '#calculate' do
    let(:fizzbuzz) { fizz+buzz }
    let(:random_number) { 7 }
    it 'given a number, returns the corresponding value' do
      expect(subject.calculate(random_number)).to eq random_number
    end
  end
end
