#!/usr/bin/env ruby
# encoding: utf-8
require 'rspec'

class FizzBuzz
  FIZZ = 'Fizz'
  BUZZ = 'Buzz'
  def initialize
  end

  def run
    1.upto(100).each do |number|
      @fizz = divisible_by?(number, 3)
      @buzz = divisible_by?(number, 5)
      puts calculate(number)
    end
  end

  def calculate(number)
    case
    when @fizz && @buzz  then FIZZ + BUZZ
    when @fizz then FIZZ
    when @buzz then BUZZ
    else number
    end
  end

  def divisible_by?(numerator, denominator)
    numerator % denominator == 0
  end
end

fizzbuzz = FizzBuzz.new
fizzbuzz.run
